from django.contrib import admin
from django.urls import path, include
from . import views
from django.views.generic import TemplateView
from .views import *

app_name='clientes_socios'

urlpatterns = [
    path('membresias/',ListarMembresias.as_view(template_name="clientes_socios/membresias_list.html"),name="membresias-list"),
    path('crear_membresia/', CrearMembresia.as_view(template_name="clientes_socios/membresia_form.html"),name="membresia-create"),
    path('detalle_membresia/<int:pk>', MembresiaDetalle.as_view(template_name="clientes_socios/membresia_detalle.html"),name="membresia-detalle"),
    path('actualizar_membresia/<int:pk>', MembresiaActualizar.as_view(template_name="clientes_socios/membresia_editar.html"),name="membresia-editar"),
    path('eliminar_membresia/<int:pk>', MembresiaEliminar.as_view(),name="membresia-eliminar"),

    path('socios/',ListarSocios.as_view(template_name="clientes_socios/socios_list.html"),name="socios-list"),
    path('crear_socio/<int:cedula>', CrearSocio.as_view(template_name="clientes_socios/socio_form.html"),name="socio-create"),
    path('detalle_socio/<int:pk>', SocioDetalle.as_view(template_name="clientes_socios/socio_detalle.html"),name="socio-detalle"),
    path('actualizar_socio/<int:pk>', SocioActualizar.as_view(template_name="clientes_socios/socio_editar.html"),name="socio-editar"),
    path('eliminar_socio/<int:pk>', SocioEliminar.as_view(),name="socio-eliminar"),

    path('registrar_cliente/<int:opcion>', RegistrarCliente.as_view(template_name="clientes_socios/cliente_form.html"),name="cliente-create"),
]