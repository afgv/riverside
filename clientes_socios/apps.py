from django.apps import AppConfig


class ClientesSociosConfig(AppConfig):
    name = 'clientes_socios'
