from django import forms
from django.contrib.auth.models import User, Permission
from django.forms.widgets import CheckboxSelectMultiple
from datetime import datetime, time, date, timedelta
from datetime import timedelta
from dateutil.relativedelta import relativedelta
from django.conf import settings
from .models import *
from users.widgets import *

class MembresiaCreateForm(forms.ModelForm):

    class Meta:
        model = Membresia
        fields = '__all__'


class SocioCreateForm(forms.ModelForm):

    cedula = forms.CharField( widget=forms.TextInput(attrs={'size': '9'}), help_text="Ingrese la cedula del cliente")
    fecha_renovacion= forms.DateField(input_formats=settings.DATE_INPUT_FORMATS)

    class Meta:
        model = Socio
        fields = ['cedula','membresia','fecha_renovacion']

    def __init__(self, cliente, *args, **kwargs):
        super(SocioCreateForm, self).__init__(*args, **kwargs)
        self.ced = cliente
        if self.ced != 0:
            self.fields['cedula'].initial=self.ced
        ahora = date.today()
        tiempo = time(0, 0)
        renovacion = ahora + relativedelta(years=1)
        renovacion = ahora + timedelta(days=365 )
        self.fields['fecha_renovacion'].initial  = datetime.combine(renovacion, tiempo).strftime('%d/%m/%Y')

    def clean_cedula(self):
        cedula = self.cleaned_data.get("cedula")

        if cedula.isdigit():
            resultado = Cliente.objects.filter(cedula=cedula).count()
            if resultado == 0:
                raise forms.ValidationError("La cedula ingresada no existe en la base de datos")
            else:
                resultado2 = Socio.objects.filter(cliente__cedula=cedula).count()
                if resultado2 == 0:
                    raise forms.ValidationError("Ya existe un socio registrado con esta cedula")
                else:
                    cliente = Cliente.objects.filter(cedula=cedula)
        else:
            raise forms.ValidationError("La cedula solamente puede contener numeros!")

class ClienteCreateForm(forms.ModelForm):

    # Ahora el campo username es de tipo email y cambiamos su texto
    nombre = forms.CharField(max_length=15)
    apellido = forms.CharField(max_length=15)
    celular = forms.CharField(max_length=12)
    direccion = forms.CharField(max_length=40)
    fecha_nacimiento = forms.DateTimeField(
        input_formats=['%d/%m/%Y'], 
        widget=CalendarioInput()
    )

    class Meta:
        model = Cliente
        fields = '__all__'

class SocioActualizar(forms.ModelForm):

    fecha_renovacion= forms.DateField(input_formats=settings.DATE_INPUT_FORMATS, widget=CalendarioInput())

    class Meta:
        model = Socio
        fields = ['membresia','fecha_renovacion']

    def __init__(self, id_socio, *args, **kwargs):
        super(SocioActualizar, self).__init__(*args, **kwargs)
        socio = Socio.objects.get(id=id_socio)
        self.fields['fecha_renovacion'].initial  = socio.fecha_renovacion.strftime('%d/%m/%Y')