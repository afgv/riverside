from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.views.generic import ListView, DetailView, UpdateView, CreateView, DeleteView
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from datetime import datetime
from .forms import *
from .models import *

class ListarMembresias(ListView):

    model = Membresia


class CrearMembresia(SuccessMessageMixin,CreateView):
    model = Membresia
    form = MembresiaCreateForm
    success_message = 'Membresia Creada Correctamente !'
    fields = '__all__'

    # Redireccionamos a la vista con la lista de servicios
    def get_success_url(self):        
        return reverse('clientes_socios:membresias-list')

class MembresiaDetalle(DetailView): 
    model = Membresia

class MembresiaActualizar(SuccessMessageMixin, UpdateView): 
    model = Membresia
    form = Membresia
    fields = "__all__"
    success_message = 'Membresia Actualizada Correctamente !'
 
    def get_success_url(self):               
        return reverse('clientes_socios:membresias-list')

class MembresiaEliminar(SuccessMessageMixin, DeleteView): 
    model = Membresia 
    form = Membresia
    fields = "__all__"     
 
    # Redireccionamos a la página principal luego de eliminar un registro o postre
    def get_success_url(self): 
        success_message = 'Membresia Eliminada Correctamente !'
        messages.success (self.request, (success_message))       
        return reverse('clientes_socios:membresias-list')


class ListarSocios(ListView):

    model = Socio


class CrearSocio(CreateView):
    model = Socio
    form_class = SocioCreateForm
    # success_message = 'Socio Creado Correctamente !'

    def form_valid(self,form):
        self.object = form.save(commit = False)
        cedula = self.kwargs.pop('cedula')
        self.object.cliente= get_object_or_404(Cliente, cedula=cedula)
        self.object.save()
        return super(CrearSocio, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(CrearSocio,self).get_form_kwargs()
        cedula = self.kwargs['cedula']
    #    # y lo envias al formulario, no se como esperas recibirlo en el formulario, pero si en el init aceptas un pk, lo envias así.
        kwargs['cliente'] = cedula
        return kwargs

    def get_success_url(self):        
        return reverse('clientes_socios:socios-list')

class SocioDetalle(DetailView): 
    model = Socio

class SocioActualizar(SuccessMessageMixin, UpdateView): 
    model = Socio
    form_class = SocioActualizar
    success_message = 'Socio Actualizado Correctamente !'
 
    def get_form_kwargs(self):
        kwargs = super(SocioActualizar,self).get_form_kwargs()
        id_socio = self.kwargs['pk']
    #    # y lo envias al formulario, no se como esperas recibirlo en el formulario, pero si en el init aceptas un pk, lo envias así.
        kwargs['id_socio'] = id_socio
        return kwargs

    def get_success_url(self):               
        return reverse('clientes_socios:socios-list')

class SocioEliminar(SuccessMessageMixin, DeleteView): 
    model = Socio 
    form = Socio
    fields = "__all__"     
    

    # Redireccionamos a la página principal luego de eliminar un registro o postre
    def get_success_url(self): 
        success_message = 'Socio Eliminado Correctamente !'
        messages.success (self.request, (success_message))       
        return reverse('clientes_socios:socios-list')

class RegistrarCliente(CreateView):
    model = Cliente
    form = ClienteCreateForm
    fields = '__all__'

    def get_success_url(self):
        opcion = self.kwargs.pop('opcion')
        cedula = self.object.cedula
        if opcion == 0:
            return reverse('clientes_socios:socio-create',kwargs={'cedula':cedula})
        else:
            return reverse('alquileres_eventos:evento-create',kwargs={'cedula':cedula})