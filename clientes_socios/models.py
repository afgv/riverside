from django.db import models

# Create your models here.
class Cliente(models.Model):

    cedula = models.BigIntegerField()
    nombre = models.CharField(max_length=30)
    apellido = models.CharField(max_length=30)
    celular = models.CharField(max_length=12, null=True)
    email = models.CharField(max_length=30, blank=True, null=True)
    direccion = models.CharField(max_length=100, null=True)
    fecha_nac = models.DateField(max_length=50, null=True)

    def __str__(self):
        return f' {self.nombre}'

class Membresia(models.Model):

    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=100, null=True)
    precio_anual = models.DecimalField(decimal_places=2, max_digits=12, help_text="Precio de la membresia")

    def __str__(self):
        return f' {self.nombre}'

class Socio(models.Model):

    cliente = models.OneToOneField(Cliente,on_delete=models.DO_NOTHING)
    membresia = models.ForeignKey(Membresia,on_delete=models.DO_NOTHING)
    fecha_renovacion = models.DateField()

    def __unicode__(self):
        return self.date