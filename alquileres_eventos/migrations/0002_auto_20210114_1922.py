# Generated by Django 3.1.5 on 2021-01-14 22:22

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('alquileres_eventos', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='alquilersector',
            name='sector',
        ),
        migrations.AddField(
            model_name='sector',
            name='alquiler',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='alquiler_sector', to='alquileres_eventos.alquilersector'),
        ),
        migrations.AlterField(
            model_name='servicio',
            name='alquiler',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='alquiler_servicio', to='alquileres_eventos.alquilersector'),
        ),
    ]
