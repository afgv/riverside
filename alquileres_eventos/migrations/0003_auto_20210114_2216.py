# Generated by Django 3.1.5 on 2021-01-15 01:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('alquileres_eventos', '0002_auto_20210114_1922'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='servicio',
            name='alquiler',
        ),
        migrations.AddField(
            model_name='alquilersector',
            name='servicio',
            field=models.ManyToManyField(to='alquileres_eventos.Servicio'),
        ),
    ]
