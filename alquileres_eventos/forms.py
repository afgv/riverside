from django import forms
from django.forms import BaseModelFormSet, BaseInlineFormSet
from django.contrib.auth.models import User, Permission
from django.forms.widgets import CheckboxSelectMultiple
from django.forms.models import inlineformset_factory
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Field, Fieldset, Div, Row, HTML, ButtonHolder, Submit
from bootstrap_datepicker_plus import DatePickerInput, TimePickerInput
from .layout_formset import *
from .models import *
from datetime import date, datetime, timedelta
from clientes_socios.models import *

import re


class PrecioServicioForm(forms.ModelForm):

    class Meta:
        model = PrecioServicioMembresia
        exclude = ('servicio',)


PreciosFormSet = inlineformset_factory(
    Servicio, PrecioServicioMembresia, form=PrecioServicioForm,
    fields=['monto', 'membresia'], extra=1, can_delete=True
    )

class PreciosBaseFormSet(BaseModelFormSet):

    def clean(self):
        cleaned_data = super(PreciosBaseFormSet, self).clean()

        lista_membresias = []

        for form in self.forms:
            if 'membresia' in form.cleaned_data:
                membresia = form.cleaned_data['membresia']
                if membresia in lista_membresias:
                    raise forms.ValidationError("Formulario invalido: Dos membresias iguales fueron seleccionadas")
                else:
                    lista_membresias.append(membresia)

            else:
                raise forms.ValidationError("Formulario invalido: No ha seleccionado un precio")

        return cleaned_data


class ServicioCreateForm(forms.ModelForm):

    class Meta:

        model = Servicio
        fields = ['nombre','descripcion','precion_normal']

    def __init__(self, *args, **kwargs):
        super(ServicioCreateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-9 create-label'
        self.helper.field_class = 'col-md-12'
        self.helper.layout = Layout(
            Div(
                Field('nombre'),
                Field('descripcion'),
                Field('precion_normal'),
                # Fieldset('Añadir Precios',
                #     Formset('precios')),
                HTML("<br>"),
                # ButtonHolder(Submit('submit', 'Guardar')),
                )
            )


class PrecioSectorForm(forms.ModelForm):

    class Meta:
        model = PrecioSectorMembresia
        exclude = ('sector',)

class SectorCreateForm(forms.ModelForm):

    class Meta:

        model = Sector
        fields = ['nombre','descripcion','precion_normal']

    def __init__(self, *args, **kwargs):
        super(SectorCreateForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-9 create-label'
        self.helper.field_class = 'col-md-12'
        self.helper.layout = Layout(
            Div(
                Field('nombre'),
                Field('descripcion'),
                Field('precion_normal'),
                # Fieldset('Añadir Precios',
                #     Formset('precios')),
                HTML("<br>"),
                # ButtonHolder(Submit('submit', 'Guardar')),
                )
            )


class EventoForm(forms.ModelForm):

    cedula = forms.CharField( widget=forms.TextInput(attrs={'size': '9'}), help_text="Ingrese la cedula del cliente")
    servicio = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=Servicio.objects.all())
    sector = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=Sector.objects.all())
    fecha_inicio = forms.DateField(
        input_formats=['%d/%m/%Y'],
        widget=DatePickerInput(format='%d/%m/%Y')
    )

    class Meta:
        model = Evento
        fields = ['cedula','nombre','descripcion','fecha_inicio','horario_inicio','fecha_fin','horario_fin']
        widgets = {
            'fecha_inicio': DatePickerInput(),
            'horario_inicio': TimePickerInput(),
            'fecha_fin': DatePickerInput(),
            'horario_fin': TimePickerInput(),
        }

    def __init__(self, cliente, *args, **kwargs):
        super(EventoForm, self).__init__(*args, **kwargs)
        self.ced = cliente
        if self.ced != 0:
            self.fields['cedula'].initial=self.ced

        self.fields['servicio'].queryset  = Servicio.objects.all()
        self.fields['sector'].queryset  = Sector.objects.all()

        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-9 create-label'
        self.helper.field_class = 'col-md-12'
        self.helper.layout = Layout(
            Div(
                Field('cedula'),
                Field('nombre'),
                Field('descripcion'),
                Field('fecha_inicio'),
                Field('horario_inicio'),
                Field('fecha_fin'),
                Field('horario_fin'),
                Field('servicio', css_class='select2-multiple'),
                Field('sector', css_class='select2-multiple'),
                # Fieldset('Añadir Precios',
                #     Formset('precios')),
                HTML("<br>"),
                ButtonHolder(Submit('submit', 'Guardar',css_class='btn btn-dark align-self-center')),
                )
            )

    # def clean_cedula(self):
    #     cedula = self.cleaned_data.get("cedula")

    #     if cedula.isdigit():
    #         resultado = Cliente.objects.filter(cedula=cedula).count()
    #         if resultado == 0:
    #             raise forms.ValidationError("La cedula ingresada no existe en la base de datos")
    #         else:
    #             resultado2 = Socio.objects.filter(cliente__cedula=cedula).count()
    #             if resultado2 == 0:
    #                 raise forms.ValidationError("Ya existe un socio registrado con esta cedula")
    #             else:
    #                 cliente = Cliente.objects.filter(cedula=cedula)
    #     else:
    #         raise forms.ValidationError("La cedula solamente puede contener numeros!")

    def clean(self):
        cedula = self.cleaned_data.get("cedula")

        if cedula.isdigit():
            resultado = Cliente.objects.filter(cedula=cedula).count()
            if resultado == 0:
                raise forms.ValidationError("La cedula ingresada no existe en la base de datos")
            else:
                resultado2 = Socio.objects.filter(cliente__cedula=cedula).count()
                if resultado2 == 0:
                    raise forms.ValidationError("Ya existe un socio registrado con esta cedula")
                else:
                    cliente = Cliente.objects.filter(cedula=cedula)
        else:
            raise forms.ValidationError("La cedula solamente puede contener numeros!")

        sectores = self.cleaned_data.get("sector")

        fecha_inicio = self.cleaned_data.get("fecha_inicio")
        horario_inicio = self.cleaned_data.get("horario_inicio")
        fecha_fin= self.cleaned_data.get("fecha_fin")
        horario_fin = self.cleaned_data.get("horario_fin")

        dia_ini = fecha_inicio.day
        mes_ini = fecha_inicio.month
        año_ini = fecha_inicio.year
        seg_ini = horario_inicio.second
        min_ini = horario_inicio.minute
        hora_ini = horario_inicio.hour

        inicio = datetime(año_ini,mes_ini,dia_ini,hora_ini,min_ini,seg_ini)

        dia_fin = fecha_fin.day
        mes_fin = fecha_fin.month
        año_fin = fecha_fin.year
        seg_fin = horario_fin.second
        min_fin = horario_fin.minute
        hora_fin = horario_fin.hour

        fin = datetime(año_fin,mes_fin,dia_fin,hora_fin,min_fin,seg_fin)

        sector_en_comun = False
        se_solapan = False
        for evento in Evento.objects.all():
            for sector in sectores:
                if sector in evento.sectores.all():
                    sector_en_comun=True

            if sector_en_comun:
                if inicio >= evento.inicio and fin <= evento.fin:
                    raise forms.ValidationError("Ya existe un evento con un sector seleccionado en la misma fecha",
                    "\nFecha Inicio: {}, Horario Inicio: {} Fecha Fin: {}, Horario Fin: {}".format(evento.fecha_inicio,evento.horario_inicio,
                    evento.fecha_fin,evento.horario_fin))


class EventoActualizarForm(forms.ModelForm):

    cedula = forms.CharField( widget=forms.TextInput(attrs={'size': '9'}), help_text="Ingrese la cedula del cliente")
    servicio = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=None)
    sector = forms.ModelMultipleChoiceField(widget=forms.CheckboxSelectMultiple(), queryset=None)
    fecha_inicio = forms.DateField(
        input_formats=['%d/%m/%Y'],
        widget=DatePickerInput(format='%d/%m/%Y')
    )

    class Meta:
        model = Evento
        fields = ['cedula','nombre','descripcion','fecha_inicio','horario_inicio','fecha_fin','horario_fin']
        widgets = {
            'fecha_inicio': DatePickerInput(),
            'horario_inicio': TimePickerInput(),
            'fecha_fin': DatePickerInput(),
            'horario_fin': TimePickerInput(),
        }

    def __init__(self, id_evento, cliente, *args, **kwargs):
        super(EventoActualizarForm, self).__init__(*args, **kwargs)
        self.ced = cliente
        if self.ced != 0:
            self.fields['cedula'].initial=self.ced

        evento = Evento.objects.get(id=id_evento)
        self.fields['servicio'].initial  = evento.servicios.all()
        self.fields['sector'].initial  = evento.sectores.all()

        self.helper = FormHelper()
        self.helper.form_tag = True
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-md-9 create-label'
        self.helper.field_class = 'col-md-12'
        self.helper.layout = Layout(
            Div(
                Field('cedula'),
                Field('nombre'),
                Field('descripcion'),
                Field('fecha_inicio'),
                Field('horario_inicio'),
                Field('fecha_fin'),
                Field('horario_fin'),
                Field('servicio', css_class='select2-multiple'),
                Field('sector', css_class='select2-multiple'),
                HTML("<br>"),
                ButtonHolder(Submit('submit', 'Guardar',css_class='btn btn-dark align-self-center')),
                )
            )

    def clean(self):
        cedula = self.cleaned_data.get("cedula")

        if cedula.isdigit():
            resultado = Cliente.objects.filter(cedula=cedula).count()
            if resultado == 0:
                raise forms.ValidationError("La cedula ingresada no existe en la base de datos")
            else:
                resultado2 = Socio.objects.filter(cliente__cedula=cedula).count()
                if resultado2 == 0:
                    raise forms.ValidationError("Ya existe un socio registrado con esta cedula")
                else:
                    cliente = Cliente.objects.filter(cedula=cedula)
        else:
            raise forms.ValidationError("La cedula solamente puede contener numeros!")

        sectores = self.cleaned_data.get("sector")

        fecha_inicio = self.cleaned_data.get("fecha_inicio")
        horario_inicio = self.cleaned_data.get("horario_inicio")
        fecha_fin= self.cleaned_data.get("fecha_fin")
        horario_fin = self.cleaned_data.get("horario_fin")

        dia_ini = fecha_inicio.day
        mes_ini = fecha_inicio.month
        año_ini = fecha_inicio.year
        seg_ini = horario_inicio.second
        min_ini = horario_inicio.minute
        hora_ini = horario_inicio.hour

        inicio = datetime(año_ini,mes_ini,dia_ini,hora_ini,min_ini,seg_ini)

        dia_fin = fecha_fin.day
        mes_fin = fecha_fin.month
        año_fin = fecha_fin.year
        seg_fin = horario_fin.second
        min_fin = horario_fin.minute
        hora_fin = horario_fin.hour

        fin = datetime(año_fin,mes_fin,dia_fin,hora_fin,min_fin,seg_fin)

        sector_en_comun = False
        se_solapan = False
        for evento in Evento.objects.all():
            for sector in sectores:
                if sector in evento.sectores.all():
                    sector_en_comun=True

            if sector_en_comun:
                if inicio >= evento.inicio and fin <= evento.fin:
                    raise forms.ValidationError("Ya existe un evento con un sector seleccionado en la misma fecha",
                    "\nFecha Inicio: {}, Horario Inicio: {} Fecha Fin: {}, Horario Fin: {}".format(evento.fecha_inicio,evento.horario_inicio,
                    evento.fecha_fin,evento.horario_fin))