from django.contrib import admin
from django.urls import path, include
from . import views
from django.views.generic import TemplateView
from .views import *

app_name='alquileres_eventos'

urlpatterns = [
    path('servicios/',ListarServicios.as_view(template_name="alquileres_eventos/servicios_list.html"),name="servicios-list"),
    path('crear_servicio/', añadirServicio,name="servicio-create"),
    path('detalle_servicio/<int:pk>', ServicioDetalle.as_view(template_name="alquileres_eventos/servicio_detalle.html"),name="servicio-detalle"),
    path('actualizar_servicio/<int:pk>', actualizarServicio, name="servicio-editar"),
    path('eliminar_servicio/<int:pk>', ServicioEliminar.as_view(),name="servicio-eliminar"),

    path('sectores/',ListarSectores.as_view(template_name="alquileres_eventos/sectores_list.html"),name="sectores-list"),
    path('crear_sector/', añadirSector,name="sector-create"),
    path('detalle_sector/<int:pk>', SectorDetalle.as_view(template_name="alquileres_eventos/sector_detalle.html"),name="sector-detalle"),
    path('actualizar_sector/<int:pk>', actualizarSector, name="sector-editar"),
    path('eliminar_sector/<int:pk>', SectorEliminar.as_view(),name="sector-eliminar"),

    path('eventos/',ListarEventos.as_view(template_name="alquileres_eventos/eventos_list.html"),name="eventos-list"),
    path('crear_evento/<int:cedula>', RegistrarEvento.as_view(template_name="alquileres_eventos/evento_form.html"),name="evento-create"),
    path('detalle_evento/<int:pk>', EventoDetalle.as_view(template_name="alquileres_eventos/evento_detalle.html"),name="evento-detalle"),
    path('actualizar_evento/<int:pk>', EventoActualizar.as_view(template_name="alquileres_eventos/evento_editar.html"), name="evento-editar"),
    path('eliminar_evento/<int:pk>', EventoEliminar.as_view(),name="evento-eliminar"),
]