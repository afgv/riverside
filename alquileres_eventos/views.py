from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.urls import reverse_lazy
from django.http import HttpResponse, HttpResponseRedirect
from django.views.generic import ListView, DetailView, UpdateView, CreateView, DeleteView
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.db import transaction
from django.forms import modelformset_factory, inlineformset_factory
from datetime import date, datetime, timedelta
from decimal import *
from .forms import *
from .models import *
 
def home (request):
    # Si estamos identificados devolvemos la portada
    if request.user.is_authenticated:
        return render(request, 'alquileres_eventos/home.html')

    return redirect('/login')


# VISTAS RELACIONADAS A SERVICIO

class ListarServicios(ListView):

    model = Servicio


class CrearServicio(SuccessMessageMixin,CreateView):
    model = Servicio
    form_class = ServicioCreateForm
    # fields = '__all__'
    success_message = 'Servicio Creado Correctamente !' # Mostramos este Mensaje luego de Crear un Servcio

    def get_context_data(self, **kwargs):
        contexto = super(CrearServicio, self).get_context_data(**kwargs)
        if self.request.POST:
            contexto['precios'] = PreciosFormSet(self.request.POST, prefix='precios')
        else:
            contexto['precios'] = PreciosFormSet(prefix='precios')
        return contexto

    def form_valid(self, form):
        contexto = self.get_context_data()
        precios = contexto['precios']
        with transaction.atomic():
            self.object = form.save()
            if precios.is_valid():
                precios.instance = self.object
                precios.save()
        return super(CrearServicio, self).form_valid(form)

    # Redireccionamos a la vista con la lista de servicios
    def get_success_url(self):        
        return reverse('alquileres_eventos:servicios-list')

def añadirServicio(request):

    PrecioServicioFormSet = modelformset_factory(PrecioServicioMembresia, form=PrecioServicioForm,
     extra=Membresia.objects.all().count(), formset=PreciosBaseFormSet)

    if request.method == "GET":
        servicio_form = ServicioCreateForm()
        formset = PrecioServicioFormSet(queryset=PrecioServicioMembresia.objects.none())
    elif request.method == "POST":
        servicio_form = ServicioCreateForm(request.POST)
        formset = PrecioServicioFormSet(request.POST, request.FILES)

        if servicio_form.is_valid() and formset.is_valid():
            servicio_obj = servicio_form.save()

            for form in formset.cleaned_data:
                if form:
                    monto = form['monto']   
                    membresia = form['membresia']
                    PrecioServicioMembresia.objects.create(monto=monto, membresia=membresia, servicio=servicio_obj)
            return HttpResponseRedirect('../servicios/')
        else:
            messages.add_message(request, messages.ERROR, 'Error!')

    return render(request, 'alquileres_eventos/servicio_form.html', {'servicio_form':servicio_form, 'formset':formset})

def actualizarServicio(request,pk):

    servicio = get_object_or_404(Servicio, id=pk)
    precios_extras = Membresia.objects.all().count() - PrecioServicioMembresia.objects.filter(servicio__id=pk).count()
    PrecioServicioFormSet = modelformset_factory(PrecioServicioMembresia, form=PrecioServicioForm,
     extra=precios_extras, formset=PreciosBaseFormSet)

    if request.method == "GET":
        pk=pk
        servicio_form = ServicioCreateForm(instance=servicio)
        formset = PrecioServicioFormSet(queryset=PrecioServicioMembresia.objects.filter(servicio__id=pk))
    elif request.method == "POST":
        pk=pk
        servicio_form = ServicioCreateForm(request.POST, instance=servicio)
        formset = PrecioServicioFormSet(request.POST, request.FILES)

        if servicio_form.is_valid() and formset.is_valid():
            servicio_obj = servicio_form.save()

            for form in formset.cleaned_data:
                if form:      
                    membresia = form['membresia']
                    if servicio_obj.precios.filter(membresia=membresia).count() == 0:
                        monto = form['monto'] 
                        PrecioServicioMembresia.objects.create(monto=monto, membresia=membresia, servicio=servicio_obj)
            formset.save()
            return HttpResponseRedirect('../servicios/')
        else:
            messages.add_message(request, messages.ERROR, 'Error!')

    return render(request, 'alquileres_eventos/servicio_editar.html', {'servicio_form':servicio_form, 'formset':formset, 'id':servicio.id})


class ServicioDetalle(DetailView): 
    model = Servicio


class ServicioEliminar(SuccessMessageMixin, DeleteView): 
    model = Servicio 
    form = Servicio
    fields = "__all__"     
 
    # Redireccionamos a la página principal luego de eliminar un registro o postre
    def get_success_url(self): 
        success_message = 'Servicio Eliminado Correctamente !'
        messages.success (self.request, (success_message))       
        return reverse('alquileres_eventos:servicios-list')


# VISTAS RELACIONADAS A SECTOR


class ListarSectores(ListView):

    model = Sector

def añadirSector(request):

    PrecioSectorFormSet = modelformset_factory(PrecioSectorMembresia, form=PrecioSectorForm,
     extra=Membresia.objects.all().count(), formset=PreciosBaseFormSet)

    if request.method == "GET":
        sector_form = SectorCreateForm()
        formset = PrecioSectorFormSet(queryset=PrecioSectorMembresia.objects.none())
    elif request.method == "POST":
        sector_form = SectorCreateForm(request.POST)
        formset = PrecioSectorFormSet(request.POST, request.FILES)

        if sector_form.is_valid() and formset.is_valid():
            sector_obj = sector_form.save()

            for form in formset.cleaned_data:
                if form:
                    monto = form['monto']   
                    membresia = form['membresia']
                    PrecioSectorMembresia.objects.create(monto=monto, membresia=membresia, sector=sector_obj)
            return HttpResponseRedirect('../sectores/')
        else:
            messages.add_message(request, messages.ERROR, 'Error!')

    return render(request, 'alquileres_eventos/sector_form.html', {'sector_form':sector_form, 'formset':formset})


def actualizarSector(request,pk):

    sector = get_object_or_404(Sector, id=pk)
    precios_extras = Membresia.objects.all().count() - PrecioSectorMembresia.objects.filter(sector__id=pk).count()
    PrecioSectorFormSet = modelformset_factory(PrecioSectorMembresia, form=PrecioSectorForm,
     extra=precios_extras, formset=PreciosBaseFormSet)

    if request.method == "GET":
        pk=pk
        sector_form = SectorCreateForm(instance=sector)
        formset = PrecioSectorFormSet(queryset=PrecioSectorMembresia.objects.filter(sector__id=pk))
    elif request.method == "POST":
        pk=pk
        sector_form = SectorCreateForm(request.POST, instance=sector)
        formset = PrecioSectorFormSet(request.POST, request.FILES)

        if sector_form.is_valid() and formset.is_valid():
            sector_obj = sector_form.save()

            for form in formset.cleaned_data:
                if form:      
                    membresia = form['membresia']
                    if sector_obj.precios.filter(membresia=membresia).count() == 0:
                        monto = form['monto'] 
                        PrecioSectorMembresia.objects.create(monto=monto, membresia=membresia, sector=sector_obj)
            formset.save()
            return HttpResponseRedirect('../sectores/')
        else:
            messages.add_message(request, messages.ERROR, 'Error!')

    return render(request, 'alquileres_eventos/sector_editar.html', {'sector_form':sector_form, 'formset':formset, 'id':sector.id})


class SectorDetalle(DetailView): 
    model = Sector

class SectorEliminar(SuccessMessageMixin, DeleteView): 
    model = Sector 
    form = Sector
    fields = "__all__"     
 
    # Redireccionamos a la página principal luego de eliminar un registro o postre
    def get_success_url(self): 
        success_message = 'Sector Eliminado Correctamente !'
        messages.success (self.request, (success_message))       
        return reverse('alquileres_eventos:sectores-list')

#EVENTOS
class ListarEventos(ListView):

    model = Evento

class RegistrarEvento(CreateView):
    model = Evento
    form_class = EventoForm
    # success_message = 'Socio Creado Correctamente !'

    def form_valid(self,form):
        self.object = form.save(commit = False)
        cedula = form.cleaned_data['cedula']
        cliente = get_object_or_404(Cliente, cedula=cedula)
        self.object.cliente = cliente

        servicios = form.cleaned_data['servicio']
        sectores = form.cleaned_data['sector']
        es_socio = Socio.objects.filter(cliente=cliente).count()
        if es_socio != 0 :
            membresia = cliente.socio.membresia
            total_servicio = Decimal(0)
            for servicio in servicios:
                monto_servicio=PrecioServicioMembresia.objects.get(servicio=servicio,membresia=membresia).monto
                total_servicio = total_servicio + monto_servicio

            total_sector = Decimal(0)
            for sector in sectores:
                monto_sector=PrecioSectorMembresia.objects.get(sector=sector,membresia=membresia).monto
                total_sector = total_sector + monto_sector
        else:
            total_servicio = Decimal(0)
            for servicio in servicios:
                total_servicio = total_servicio + servicio.precion_normal

            total_sector = Decimal(0)
            for sector in sectores:
                total_sector = total_sector + sector.precion_normal

        total_evento = total_servicio + total_sector

        self.object.usuario_registro = self.request.user.perfil
        self.object.precio_total = total_evento

        self.object.servicios.set(servicios)
        self.object.sectores.set(sectores)
        self.object.save()
        form.save_m2m()
        return super(RegistrarEvento, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(RegistrarEvento,self).get_form_kwargs()
        cedula = self.kwargs['cedula']
    #    # y lo envias al formulario, no se como esperas recibirlo en el formulario, pero si en el init aceptas un pk, lo envias así.
        kwargs['cliente'] = cedula
        return kwargs

    def get_success_url(self):        
        return reverse('alquileres_eventos:eventos-list')


class EventoActualizar(SuccessMessageMixin, UpdateView): 
    model = Evento
    form_class = EventoForm
    # fields = "__all__"
    success_message = 'Evento Actualizado Correctamente !'

    def form_valid(self,form):
        self.object = form.save(commit = False)
        cedula = form.cleaned_data['cedula']
        cliente = get_object_or_404(Cliente, cedula=cedula)
        self.object.cliente = cliente

        servicios = form.cleaned_data['servicio']
        sectores = form.cleaned_data['sector']
        es_socio = Socio.objects.filter(cliente=cliente).count()
        if es_socio != 0 :
            membresia = cliente.socio.membresia
            total_servicio = Decimal(0)
            for servicio in servicios:
                monto_servicio=PrecioServicioMembresia.objects.get(servicio=servicio,membresia=membresia).monto
                total_servicio = total_servicio + monto_servicio

            total_sector = Decimal(0)
            for sector in sectores:
                monto_sector=PrecioSectorMembresia.objects.get(sector=sector,membresia=membresia).monto
                total_sector = total_sector + monto_sector
        else:
            total_servicio = Decimal(0)
            for servicio in servicios:
                total_servicio = total_servicio + servicio.precion_normal

            total_sector = Decimal(0)
            for sector in sectores:
                total_sector = total_sector + sector.precion_normal

        total_evento = total_servicio + total_sector

        self.object.usuario_registro = self.request.user.perfil
        self.object.precio_total = total_evento

        self.object.servicios.set(servicios)
        self.object.sectores.set(sectores)
        self.object.save()
        form.save_m2m()
        return super(EventoActualizar, self).form_valid(form)

    def get_form_kwargs(self):
        kwargs = super(EventoActualizar,self).get_form_kwargs()
    #    # y lo envias al formulario, no se como esperas recibirlo en el formulario, pero si en el init aceptas un pk, lo envias así.
        id_evento = cedula = self.kwargs['pk']
        kwargs['cliente'] = self.object.cliente.cedula
        return kwargs
 
    def get_success_url(self):               
        return reverse('alquileres_eventos:eventos-list')


class EventoDetalle(DetailView): 
    model = Evento


class EventoEliminar(SuccessMessageMixin, DeleteView): 
    model = Evento 
    form = Evento
    fields = "__all__"     
 
    # Redireccionamos a la página principal luego de eliminar un registro o postre
    def get_success_url(self): 
        success_message = 'Evento Eliminado Correctamente !'
        messages.success (self.request, (success_message))       
        return reverse('alquileres_eventos:eventos-list')
