from django.apps import AppConfig


class AlquileresEventosConfig(AppConfig):
    name = 'alquileres_eventos'
