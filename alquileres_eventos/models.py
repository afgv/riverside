from django.db import models
from clientes_socios.models import *
from users.models import *
from datetime import date, datetime, timedelta

# Create your models here.

class SectorServicio(models.Model):

    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=50,null=True)
    precion_normal = models.DecimalField(decimal_places=2, max_digits=12)

    def __str__(self):
        return f' {self.nombre}'

class Sector(SectorServicio):

    # alquiler = models.ForeignKey(AlquilerSector, related_name='sectores', null=True, on_delete=models.DO_NOTHING)

    def __str__(self):
        return f'Sector: {self.nombre}'

class Servicio(SectorServicio):
    
    def __str__(self):
        return f'Servicio: {self.nombre}'

class Evento(models.Model):

    nombre = models.CharField(max_length=30)
    descripcion = models.CharField(max_length=50,null=True)
    precio_total = models.DecimalField(decimal_places=2, max_digits=12)
    cliente = models.ForeignKey(Cliente, related_name='eventos', on_delete=models.DO_NOTHING)
    usuario_registro = models.ForeignKey(Perfil, related_name='eventos', on_delete=models.DO_NOTHING)
    fecha_inicio = models.DateField()
    horario_inicio = models.TimeField()
    fecha_fin = models.DateField()
    horario_fin = models.TimeField()
    inicio = models.DateTimeField(blank=True, null=True)
    fin = models.DateTimeField(blank=True, null=True)
    sectores = models.ManyToManyField(Sector)
    servicios = models.ManyToManyField(Servicio)

    def save(self, *args, **kwargs):
        # Check how the current values differ from ._loaded_values. For example,
        # prevent changing the creator_id of the model. (This example doesn't
        # support cases where 'creator_id' is deferred).
        dia_ini = self.fecha_inicio.day
        mes_ini = self.fecha_inicio.month
        año_ini = self.fecha_inicio.year
        seg_ini = self.horario_inicio.second
        min_ini = self.horario_inicio.minute
        hora_ini = self.horario_inicio.hour

        self.inicio = datetime(año_ini,mes_ini,dia_ini,hora_ini,min_ini,seg_ini)

        dia_fin = self.fecha_fin.day
        mes_fin = self.fecha_fin.month
        año_fin = self.fecha_fin.year
        seg_fin = self.horario_fin.second
        min_fin = self.horario_fin.minute
        hora_fin = self.horario_fin.hour

        self.fin = datetime(año_fin,mes_fin,dia_fin,hora_fin,min_fin,seg_fin)

        super().save(*args, **kwargs)

# class AlquilerSector(models.Model):

#     nombre = models.CharField(max_length=30)
#     descripcion = models.CharField(max_length=50,null=True)
#     precion_normal = models.DecimalField(decimal_places=2, max_digits=12)
#     # sector = models.ForeignKey(Sector, related_name='sector_alquilado', on_delete=models.DO_NOTHING)
#     evento = models.ForeignKey(Evento, related_name='alquileres', on_delete=models.DO_NOTHING)
#     servicio = models.ManyToManyField(Servicio)

class PrecioMembresia(models.Model):

    monto = models.DecimalField(decimal_places=2, max_digits=12)
    membresia = models.ForeignKey(Membresia, related_name='precios_servicios', on_delete=models.DO_NOTHING)

    def __str__(self):
        return f'{self.monto} con la membresia {self.membresia}'

class PrecioServicioMembresia(PrecioMembresia):

    servicio = models.ForeignKey(Servicio, related_name='precios', on_delete=models.CASCADE)

class PrecioSectorMembresia(PrecioMembresia):

    sector = models.ForeignKey(Sector, related_name='precios', on_delete=models.CASCADE)


# class PrecioAlquilerMembresia(PrecioMembresia):

#     alquiler_sector = models.ForeignKey(AlquilerSector, related_name='precios', on_delete=models.CASCADE)
