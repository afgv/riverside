from django.contrib import admin
from .models import *

# Register your models here.
admin.site.register(Servicio)
admin.site.register(Sector)
# admin.site.register(AlquilerSector)
admin.site.register(Evento)
admin.site.register(PrecioServicioMembresia)
admin.site.register(PrecioSectorMembresia)
# admin.site.register(PrecioAlquilerMembresia)