from django.shortcuts import render, redirect, reverse, get_object_or_404
from django.views.generic import ListView, DetailView, UpdateView, CreateView, DeleteView
from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login, authenticate, logout
from django.contrib.auth.mixins import LoginRequiredMixin
from guardian.mixins import PermissionRequiredMixin, PermissionListMixin
from django.http import HttpResponse, HttpResponseRedirect
from .forms import *
from .models import *


def registrar_usuario(request, backend='django.contrib.auth.backends.ModelBackend'):

    form = RegistroEmail()

    if request.method == "POST":
        form = RegistroEmail(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():

            # Creamos la nueva cuenta de usuario
            user = form.save()

            nombre = form.cleaned_data['nombre']
            apellido = form.cleaned_data['apellido']
            celular = form.cleaned_data['celular']
            direccion = form.cleaned_data['direccion']
            nacimiento = form.cleaned_data['fecha_nacimiento']
            email = form.cleaned_data['email']

            user.first_name = nombre
            user.last_name = apellido
            user.is_active = True
            user.save()
            Perfil.objects.create(user=user, celular=celular, direccion=direccion, fecha_nac=nacimiento, email=email)
            # Si el usuario se crea correctamente 
            # if user is not None:
            #     # Hacemos el login manualmente
            #     login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            #     # Y le redireccionamos a la portada
            #     return redirect('/')

    # Si llegamos al final renderizamos el formulario
    # return render(request, 'acceso/register.html', {'form_user': form_user, 'form_perfil': form_perfil})
    return render(request, 'users/registrar_usuario.html', {'form': form})

def login_usuario(request):
    # Creamos el formulario de autenticación vacío
    form = LoginEmail()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = LoginEmail(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Recuperamos las credenciales validadas
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            # Verificamos las credenciales del usuario
            user = authenticate(username=username, password=password)

            # Si existe un usuario con ese nombre y contraseña
            if user is not None:
                # Hacemos el login manualmente
                login(request, user)
                # Y le redireccionamos a la portada
                return redirect('/')

    # Si llegamos al final renderizamos el formulario
    return render(request, "users/login.html", {'form': form})


def salir(request):
    # Finalizamos la sesión
    logout(request)
    # Redireccionamos a la portada
    return redirect('/')

# class ListarRoles(ListView):

#     model = Rol

# class CrearRol(SuccessMessageMixin,CreateView):
#     model = Rol
#     # form = ServicioCreateForm
#     fields = '__all__'
#     success_message = 'Rol Creado Correctamente !' # Mostramos este Mensaje luego de Crear un Departamento

#     # Redireccionamos a la vista con la lista de Departamentos
#     def get_success_url(self):        
#         return reverse('users:roles-list')

# class RolDetalle(DetailView): 
#     model = Rol

# class RolActualizar(SuccessMessageMixin, UpdateView): 
#     model = Rol
#     form = Rol
#     fields = '__all__'
#     success_message = 'Rol Actualizada Correctamente !'
 
#     def get_success_url(self):               
#         # return reverse('alquileres_eventos:servicio-detalle')
#         return reverse('users:roles-list')

# class RolEliminar(SuccessMessageMixin, DeleteView): 
#     model = Rol 
#     form = Rol
#     fields = "__all__"     
 
#     # Redireccionamos a la página principal luego de eliminar un registro o postre
#     def get_success_url(self): 
#         success_message = 'Rol Eliminado Correctamente !'
#         messages.success (self.request, (success_message))       
#         return reverse('users:roles-list')

class UserListView(LoginRequiredMixin, ListView):
    """
    Vista de lista de usuarios
    """
    login_url = ''
    template_name = "users/user_list.html"
    model = User
    
    def get_queryset(self):
        """
        Se muestra el usuario que se recibio
        por el form haciendo un get del query a los
        objetos users
        """
        query = self.request.GET.get('username')
        
        if query:
            object_list = self.model.objects.filter(username__icontains=query)
            if object_list.count() == 0:
                messages.error(self.request, 'Usuario no encontrado')
        else:
            object_list = self.model.objects.all()
        
        return object_list.order_by('username')


# class RolUpdateView(UpdateView):
#     """
#     Añade un rol general de sistema a un usuario en especifico
#     """
#     form_class= AnadirRolForm
#     model = Perfil
#     template_name = 'users/anadir_rol.html'
#     #queryset = Rol.objects.all()
#     #permission_required = ['view_rol']

#     def get_object(self):
#         id_ = self.kwargs.get("id")
#         return get_object_or_404(Perfil, id=id_)

#     #def get_initial(self, *args, **kwargs):
#     #    initial = super(RolUpdateView, self).get_initial(**kwargs)
#     #    initial['roles'] =  Rol.objects.all()
#     #    return initial

#     def get_context_data(self, **kwargs):
#         contexto = super(RolUpdateView, self).get_context_data(**kwargs)
#         #contexto['roles'] = get_object_or_404(Rol, id=self.kwargs['id'])
#         #contexto['usuario'] = get_object_or_404(User, id=self.kwargs['id'])
#         return contexto

#     def form_valid(self,form):
#          self.object = form.save(commit = False)
#          roles_=form.cleaned_data.get('roles')
#          self.object.roles.set(roles_)
#          for rol in roles_.all():
#              for permiso in rol.permisos.all():
#                  self.object.user.user_permissions.add(permiso)
#          #self.object.roles = self.request.POST.getlist('roles')
#          #print(request.POST.get('roles'))
#          self.object.save()
#          return super(RolUpdateView, self).form_valid(form)

def habilitarVista(request,pk,opcion=None):
    """
    Añade o quita el permiso de ver sitios a los usuarios, y tambien su condicion de staff del sistema
    param id: Es el id al que se le va a añadir o quitar permisos
    param opcion: Igual a 1 si se le va a dar el permiso de ver sitios y hacerle staff del sistema
    param opcion: Igual a 2 si se le va a quitar el permiso de ver sitios y quitarle la condicion de staff del sistema
    """
    if request.method=='POST':
        context={
        }
        estado=False
        usuario=User.objects.get(id=pk)
        print("El usuario recibido es: ",usuario)
        verSitio_perm=Permission.objects.get(codename='view_site')
        if(opcion==1):
            usuario.user_permissions.add(verSitio_perm)
            usuario.is_staff=True
        else:
            usuario.user_permissions.remove(verSitio_perm)
            usuario.is_staff=False
        usuario.save()
        estado=False
        context={        
                "estado":estado
        }
        return render(request,'user-lista',context)
        # return HttpResponseRedirect(reverse('users:user-lista'))


class UserDetailView(LoginRequiredMixin, DetailView):
    """
    Vista de detalles de Usuario
    """
    login_url = ''
    template_name = 'user_detail'

    def get_object(self):
        id_ = self.kwargs.get("pk")
        return get_object_or_404(User, id=id_)

class UserUpdateView(LoginRequiredMixin, UpdateView):
    """
    Vista de actualizacion del perfil
    """
    template_name = 'users/user_update.html'
    fields = ['direccion','fecha_nac','celular','email']

    def get_object(self):
        """
        Retorna el objecto a usarse, en este caso
        el perfil del usuario
        """
        user = get_object_or_404(User, id=self.kwargs['pk'])
        return user.perfil

class UserEliminar(SuccessMessageMixin, DeleteView): 
    model = User 
    form = User
    fields = "__all__"     
 
    # Redireccionamos a la página principal luego de eliminar un registro o postre
    def get_success_url(self): 
        success_message = 'Usuario Eliminado Correctamente !'
        messages.success (self.request, (success_message))       
        return reverse('users:user-lista')