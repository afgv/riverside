from django.contrib import admin
from django.urls import path, include
from . import views
from django.views.generic import TemplateView
from .views import *

app_name='users'

urlpatterns = [
    path('usuarios/', UserListView.as_view(template_name="users/user_list.html"), name='user-lista'),

    # path('lista_roles/',ListarRoles.as_view(template_name="users/roles_list.html"),name="roles-lista"),
    # path('crear_rol/', CrearRol.as_view(template_name="users/rol_form.html"),name="rol-crear"),
    # path('detalle_rol/<int:pk>', RolDetalle.as_view(template_name="users/rol_detalle.html"),name="rol-detalle"),
    # path('actualizar_rol/<int:pk>', RolActualizar.as_view(template_name="users/rol_editar.html"),name="rol-editar"),
    # path('eliminar_rol/<int:pk>', RolEliminar.as_view(),name="rol-eliminar"),

    
    path('<int:pk>/', UserDetailView.as_view(template_name="users/user_detail.html"), name='user-detalle'),
    path('<int:pk>/update', UserUpdateView.as_view(template_name="users/user_update.html"), name='user-editar'),
    path('eliminar_usuarios/<int:pk>', UserEliminar.as_view(),name="user-eliminar"),
    # path('<int:pk>/roles', RolUpdateView.as_view(template_name="users/anadir_rol.html"),
    # name="anadir-rol"),
    path('habilitar/<int:pk>/<int:opcion>/', views.habilitarVista, name="habilitar-staff"),
]