from django.db import models
from django.contrib.auth.models import User, Permission, Group

# class Rol(models.Model):

#         nombre = models.CharField(max_length=50)
#         descripcion = models.CharField(max_length=100, null=True, blank=True)
#         permisos = models.ManyToManyField(Permission)


class Perfil(models.Model):

	ESTADOS =(
			('Activo', 'Activo'),
			('Inactivo','Inactivo'),
			)

	user = models.OneToOneField(User, on_delete=models.CASCADE, null=True)
	celular = models.CharField(max_length=12, null=True)
	email = models.CharField(max_length=30, null=True)
	direccion = models.CharField(max_length=100, null=True)
	fecha_nac = models.DateField(max_length=50, null=True)
	estado = models.CharField(max_length=50, null=True, choices=ESTADOS, default=ESTADOS[0][0])
	verificado = models.BooleanField(default=False)
	es_gerente = models.BooleanField(default=False)

	def __str__(self):
		return f'{ self.user.username }'