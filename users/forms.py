from django import forms
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth.models import User
from users.widgets import *
from users.models import *

# Extendemos del original
class  RegistroEmail(UserCreationForm):
    # Ahora el campo username es de tipo email y cambiamos su texto
    nombre = forms.CharField(max_length=12)
    apellido = forms.CharField(max_length=12)
    celular = forms.CharField(max_length=12)
    direccion = forms.CharField(max_length=30)
    # fecha_nacimiento = forms.DateTimeField(
    #     input_formats=['%d/%m/%Y'], 
    #     widget=BootstrapDateTimePickerInput()
    # )
    fecha_nacimiento = forms.DateTimeField(
        input_formats=['%d/%m/%Y'], 
        widget=CalendarioInput()
    )

    class Meta:
        model = User
        fields = ["username", "nombre", "apellido", "email", "password1", "password2", "celular", "direccion", "fecha_nacimiento"]


# Extendemos del original
class LoginEmail(AuthenticationForm):
    # Ahora el campo username es de tipo email y cambiamos su texto
    # username = forms.EmailField(label="Correo electrónico")

    class Meta:
        model = User
        fields = ["username", "password"]

# class AnadirRolForm(forms.ModelForm):
#     def __init__(self, *args, **kwargs):
#         super(AnadirRolForm, self).__init__(*args, **kwargs)
#         self.fields['roles'].queryset =  Rol.objects.filter(es_desistema=True)

#     class Meta:
#         model= Perfil
#         fields= ['roles']
#         widgets = {
#         'roles' : forms.CheckboxSelectMultiple()}